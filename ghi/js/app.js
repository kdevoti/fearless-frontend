window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, starts, ends, location) {

        return `
            <div class="col-md-4">
                <div class="card">
                    <img src="${pictureUrl}" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">${name}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                        <p class="card-text">${description}</p>
                    <div class="card-footer">
                        ${starts} - ${ends}
                    </div>
                    </div>
                </div>
            </div>
        `;
    }

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Error: ' + response.status);

        } else {
            const data = await response.json();
            const column = document.querySelector('.row')

            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name;

                    const html = createCard(
                        name,
                        description,
                        pictureUrl,
                        starts,
                        ends,
                        location,
                    );

                    const div = document.createElement('div');
                    div.innerHTML = html.trim();
                    column.appendChild(div.firstChild);
                }
            }
        }

    } catch (e) {
        console.error("An error has occured")
    }

});